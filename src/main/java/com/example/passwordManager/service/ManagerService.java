package com.example.passwordManager.service;


import com.example.passwordManager.model.Manager;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

public interface ManagerService {
    List<Manager> getManagerRecords();
    void saveManagerRecord(Manager manager);
    void deleteManagerRecord(long id);
    Manager getManagerRecord(long id);
    void deleteAllManagerRecords();
    List<Manager> getManagerRecordsBySiteName(Manager manager);
    Sort sortBySiteName(Manager manager);
}
