package com.example.passwordManager.service;

import com.example.passwordManager.model.Manager;
import com.example.passwordManager.repository.ManagerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ManagerServiceImpl implements ManagerService {

    @Autowired
    ManagerRepository managerRepository;

    @Override
    @Transactional
    public List<Manager> getManagerRecords() {
        return managerRepository.findAll();
    }

    @Override
    @Transactional
    public void saveManagerRecord(Manager manager) {
        managerRepository.save(manager);
    }

    @Override
    public void deleteManagerRecord(long id) {
        managerRepository.delete(getManagerRecord(id));
    }

    @Override
    public Manager getManagerRecord(long id) {
        return managerRepository.findById(id);
    }

    @Override
    public void deleteAllManagerRecords() {
        managerRepository.deleteAll();
    }

    @Override
    public List<Manager> getManagerRecordsBySiteName(Manager manager) {
        return managerRepository.findAll(sortBySiteName(manager));
    }

    @Override
    public Sort sortBySiteName(Manager manager) {
        return Sort.by("siteName");
    }
}
