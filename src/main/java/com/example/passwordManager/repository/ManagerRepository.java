package com.example.passwordManager.repository;

import com.example.passwordManager.model.Manager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManagerRepository extends JpaRepository<Manager, Long> {
   Manager findById(long id);
}
