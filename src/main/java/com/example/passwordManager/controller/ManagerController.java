package com.example.passwordManager.controller;

import com.example.passwordManager.model.Manager;
import com.example.passwordManager.service.ManagerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/manager")
@Controller
public class ManagerController {
    @Autowired
    ManagerServiceImpl managerService;

    @GetMapping("/display")
        public String managerRecordsDisplay(Model model){

            model.addAttribute("managerRecordsList", managerService.getManagerRecords());
            return "managerRecordsDisplay";
        }

    @GetMapping("/createRecord")
    public String createRecord(Model model){
        Manager manager = new Manager();
        model.addAttribute("manager", manager);
        model.addAttribute("managerRecordsList", managerService.getManagerRecords());
        return "addManagerRecord";
    }

    @PostMapping("/saveRecord")
    public String saveRecord(@ModelAttribute("manager") Manager manager){
        managerService.saveManagerRecord(manager);
        return "redirect:/manager/display";
    }

    @RequestMapping("/deleteRecord")
    public String deleteRecord(@RequestParam("id")long id){
        managerService.deleteManagerRecord(id);
        return "redirect:/manager/display";
    }

    @GetMapping("/updateRecord")
    public String updateRecord(@RequestParam("id")long id, Model model){
        Manager manager = managerService.getManagerRecord((id));
        model.addAttribute("manager", manager);
        return "addManagerRecord";
    }

    @RequestMapping("/deleteAllRecords")
    public String deleteAllRecords(){
        managerService.deleteAllManagerRecords();
        return "redirect:/manager/display";
    }

    @GetMapping("/displayBySiteName")
    public String displayBySiteName(Model model, Manager manager){

        model.addAttribute("managerRecordsList", managerService.getManagerRecordsBySiteName(manager));
        return "displaySortedBySiteName";
    }
}
